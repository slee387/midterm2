// Eliza Cohn (ecohn4)
// Soo Hyun Lee (slee387)
// project.c
// 601.220, Spring 2019

#include "ppm_io.h"
#include "imageManip.h"
#include <stdlib.h>
#include <string.h>

int main(int argc, char * argv[]) {

    // If not enough arguments are provided by the user, report error.
    if (argc < 3) {
        fprintf(stderr, "not enough files provided\n");
        return 1;
    }

    // Open the binary file
    FILE *fp = fopen(argv[1], "rb");

    // If the reading fails, report error.
    if (!fp) {
        fprintf(stderr, "invalid filename read\n");
        return 2;
    }

    // Read ppm file into an Image.
    Image * img = read_ppm(fp);

    // If image isn't read, report error.
    if (!img) {
        fprintf(stderr, "reading file failed\n");
        return 3;
    }

    // If there are fewer than 4 arguments, there would be no operations.
    // Report error.
    if (argc < 4) {
        fprintf(stderr, "no operation specified\n");
        return 4;
    }

    // If the operation is swap
    if (strcmp(argv[3], "swap") == 0) {

        // If the number of arguments isn't correct (only accepting 'swap')
        if (argc != 4) {
            fprintf(stderr, "incorrect number of arguments\n");
            return 5;
        }
        img = swap(img);

    // If the operation is bright
    } else if (strcmp(argv[3], "bright") == 0) {
        
        // Need operation name and the level of brightness 
        if (argc != 5) {
            fprintf(stderr, "incorrect number of arguments\n");
            return 5;
        }

        double send;
        sscanf(argv[4], "%lf", &send);

        img = bright(img, send);

    // If the operation is invert
    } else if (strcmp(argv[3], "invert") == 0) {
        
        // Just need the operation name
        if (argc != 4) {
            fprintf(stderr, "incorrect number of arguments\n");
            return 5;
        }
        img = invert(img);

    // If the operation is gray
    } else if (strcmp(argv[3], "gray") == 0) {
        
        // Just need the operation name
        if (argc != 4) {
            fprintf(stderr, "incorrect number of arguments\n");
            return 5;
        }
        img = grayscale(img);

    // If the operation is crop
    } else if (strcmp(argv[3], "crop") == 0) {
        
        // Need operation name and 4 integer values
        if (argc != 8) {
            fprintf(stderr, "incorrect number of arguments\n");
            return 5;
        }

        int top1, top2, bot1, bot2;

        sscanf(argv[4], "%d", &top1);
        sscanf(argv[5], "%d", &top2);
        sscanf(argv[6], "%d", &bot1);
        sscanf(argv[7], "%d", &bot2);

        // Can't handle negatives
        if (top1 < 0 || top2 < 0 || bot1 < 0 || bot2 < 0) {
            fprintf(stderr, "arguments out of range\n");
            return 6;
        }

        // These are invalid ranges
        if (top1 > bot1 || top2 > bot2) {
            fprintf(stderr, "arguments out of range\n");
            return 6;
        }

        // These are invalid ranges that produce size 0 images
        if (bot1 - top1 <= 0 || bot2 - top2 <= 0) {
            fprintf(stderr, "cropping to 0 is invalid\n");
            return 6;
        }

        img = crop(img, top1, top2, bot1, bot2);

    // If the operation is blur
    } else if (strcmp(argv[3], "blur") == 0) {
        
        // Need operation name and the level of blur
        if (argc != 5) {
            fprintf(stderr, "incorrect number of arguments\n");
            return 5;
        }

        double sigma;
        sscanf(argv[4], "%lf", &sigma);

	if (sigma < 0 ) {
	    sigma *= -1;
	}
	
        img = blur(img, sigma);

    // If the operation is edges
    } else if (strcmp(argv[3], "edges") == 0) {
        
        // Need operation name and two values for creating edges
        if (argc != 6) {
            fprintf(stderr, "incorrect number of arguments\n");
            return 5;
        }

        double sig, thresh;
        sscanf(argv[4], "%lf", &sig);
        sscanf(argv[5], "%lf", &thresh);

        // Cannot handle negative threshold values
        if (thresh < 0) {
            fprintf(stderr, "threshold cannot be negative\n");
            return 6;
        }

        img = edgeDetection(img, sig, thresh);

    } else {

        fprintf(stderr, "operation name specifier(s) invalid\n");
        return 4;

    }

    char * check = argv[2];
    int len = strlen(check);

    // asking the user to write to a ppm file 
    if (len < 5 || check[len - 4] != '.' || check[len - 3] != 'p' || check[len - 2] != 'p' || check[len - 1] != 'm') {
        fprintf(stderr, "please write to a ppm file\n");
        return 7;
    }
      
    FILE *fp2 = fopen(argv[2], "wb");

    if (!fp2) {
        fprintf(stderr, "output file couldn't be opened for writing\n");
        return 7;
    }


    int pixels = write_ppm(fp2, img);
    if (pixels == -1) {
        fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");  
        return 7;
    }


    free(img->data);
    free(img);
    //printf("Number of pixels written: %d\n", pixels);

    return 0;
}
