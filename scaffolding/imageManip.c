// Soo Hyun Lee (slee387)
// Eliza Cohn (ecohn4)
// imageManip.c
// 601.220, Spring 2019

#include <stdlib.h>
#include "ppm_io.h"
#include <math.h>

/**
  * This function swaps the R, G, B values (R -> G, G -> B, B -> R)
  * to produce different shades of the image.
  * 
  * @param im image to swap.
  * @return modified image. 
  */
Image * swap(Image * im) {

    unsigned char red, green, blue;

    for (int rows = 0; rows < im->y; rows++) {

        for (int cols = 0; cols < im->x; cols++) {
            red = im->data[(rows * im->x + cols)].r;
            green = im->data[(rows * im->x + cols)].g;
            blue = im->data[(rows * im->x + cols)].b;

            im->data[(rows * im->x + cols)].r = green;
            im->data[(rows * im->x + cols)].g = blue;
            im->data[(rows * im->x + cols)].b = red;

        }
    }

    return im;
}

/**
  * This helper function clamps the rgb values to [0,255].
  * 
  * @param input char to check.
  * @param level the brightness level user wants to adjust.
  * @return value of brightness to apply to the image.
  */
int saturation_check(unsigned char input, double level) {

    int calc = input + level;

    if (calc > 255) { 
        return 255;

    } else if (calc < 0) {
        return 0;

    } else {
        return calc;
    }

}

/**
  * This function adjusts the brightness of the image 
  * based on user input.
  * 
  * @param im image to modify the brightness of.
  * @param level the brightness level user wants to adjust.
  * @return modified image.
  */
Image * bright(Image * im, double level) {

    Image * retIm = malloc(sizeof(Image));
    Pixel * pix = malloc(sizeof(Pixel) * im->x * im->y);
    retIm->data = pix;
    retIm->x = im->x;
    retIm->y = im->y;

    for (int rows = 0; rows < im->y; rows++) {

        for (int cols = 0; cols < im->x; cols++) {

            retIm->data[(rows * im->x + cols)].r = saturation_check(im->data[(rows * im->x + cols)].r, level);
            retIm->data[(rows * im->x + cols)].g = saturation_check(im->data[(rows * im->x + cols)].g, level);
            retIm->data[(rows * im->x + cols)].b = saturation_check(im->data[(rows * im->x + cols)].b, level);
        }
    }

    return retIm;
}

/**
  * This function inverts the colors of the image.
  * 
  * @param im image to invert.
  * @return modified image.
  */
Image * invert(Image * im) {

    for (int rows = 0; rows < im->y; rows++) {

        for (int cols = 0; cols < im->x; cols++) {
            im->data[(rows * im->x + cols)].r = 255 - im->data[(rows * im->x + cols)].r;
            im->data[(rows * im->x + cols)].g = 255 - im->data[(rows * im->x + cols)].g;
            im->data[(rows * im->x + cols)].b = 255 - im->data[(rows * im->x + cols)].b;
        }
    }

    return im;
}

/**
  * This function changes the image to grayscale.
  * 
  * @param img image to modify.
  * @return modified image.
  */
Image * grayscale(Image * img) {

    double intense;  
    for (int r = 0; r < img->y; r++) {

        for (int c = 0; c < img->x; c++) {

            intense = ((img->data[(r * img->x) + c].r)*0.3) +
                      ((img->data[(r * img->x) + c].g)*0.59) +
                      ((img->data[(r * img->x) + c].b)*0.11);

            img->data[(r * img->x) + c].r = intense;
            img->data[(r * img->x) + c].g = intense;
            img->data[(r * img->x) + c].b = intense; 
        }
    }

   return img;
  
}

/**
  * This function crops the image based on user input.
  * 
  * @param im image to modify.
  * @param topCol the starting column value.
  * @param topRow the starting row value.
  * @param botCol the bottom column value.
  * @param botRow the bottom row value.
  * @return modified image.
  */
Image * crop(Image * im, int topCol, int topRow, int botCol, int botRow) {

    int newRows = botRow - topRow;
    int newCols = botCol - topCol;

    Image * retIm = malloc(sizeof(Image));
    Pixel * pix = malloc(sizeof(Pixel) * newRows * newCols);
    retIm->data = pix;
    retIm->x = newCols;
    retIm->y = newRows;

    for (int rows = 0; rows < newRows; rows++) {

        for (int cols = 0; cols < newCols; cols++) {

            retIm->data[(rows * newCols + cols)].r = im->data[((topRow + rows) * im->x + topCol + cols)].r;
            retIm->data[(rows * newCols + cols)].g = im->data[((topRow + rows) * im->x + topCol + cols)].g;
            retIm->data[(rows * newCols + cols)].b = im->data[((topRow + rows) * im->x + topCol + cols)].b;

        }
    }

    free(im->data);
    free(im);

    return retIm;
  
}


/**
  * This function creates the square of a value
  * 
  * @param the value square.
  * @return the result.
  */
double sq(double num) {
    return num * num;
}

/**
  * This function creates the guassian matrix
  * 
  * @param the sigma value.
  * @return a pointer to the gaussian matrx.
  */
double ** gaussian(double sigma) {

    const double PI = 3.14159265358979323846264338;
  
    int N = sigma * 10;

    if (N % 2 == 0) {
        N++;
    }

    double **G = malloc(sizeof(double *) * N);

    for (int i = 0; i < N; i++) {
        G[i] = malloc(sizeof(double) * N);
    }
  
    const double c = 1.0/(2.0 * PI * sq(sigma));
    double temp;
    int dx, dy;
  
    for (int i = 0; i < N; i++) {

        for (int j = 0; j < N; j++) {

            dx = (N/2) - i;
            dy = (N/2) - j;
            temp = -((sq(dx) + sq(dy)) / (2 * sq(sigma)));
            G[i][j] = c * exp(temp);
        }
    }

    return G;
}


/**
  * This function creates the filter response for a single pixel
  * 
  * @param the gaussina matrix.
  * @param the input image pointer
  * @param the row and col location of the pixel 
  * @param the pigment color, r g or b
  * @param the sigma value
  * @return the new blurred value for the pixel.
  */
int filterResponse(double ** g, const Image * img, int r, int c, char t,
		   double sigma) {

    double sum = 0;
    double weight_sum = 0;

    int N = sigma * 10;
    if (N % 2 == 0) {
        N++;
    }

    int imr_s, imc_s, filr_s, filc_s; 
    int filr_e, filc_e; 
  
    if ((N/2) > r) {
        imr_s = 0;
        filr_s = r;

    } else {
        filr_s = 0;
        imr_s = r - (N/2);
    }

    if ((N/2) > c) {
        imc_s = 0;
        filc_s = c;

    } else {
        filc_s = 0;
        imc_s = c - (N/2);
    }

    if ((N/2) > img->y - r){
        filr_e = img->y - r;

    } else {
        filr_e = N;
    }

    if ((N/2) > img->x - c){
        filc_e = img->x - c;

    } else {
        filc_e = N;
    }

    int R = filr_e - filr_s - 1;
    int C = filc_e - filc_s - 1;

    if (t == 'r') {
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                sum += g[i][j]*img->data[((imr_s + i) * img->x) + (imc_s + j)].r;
	              weight_sum += g[i][j];
            }
        }
    }

    if (t == 'b') {
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                sum += g[i][j]*img->data[((imr_s + i) * img->x) + (imc_s + j)].b;
	              weight_sum += g[i][j];
            }
        }
    }

    if (t == 'g') {
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                sum += g[i][j]*img->data[((imr_s + i) * img->x) + (imc_s + j)].g;
	              weight_sum += g[i][j];
            }
        }
    } 

    int val = sum/weight_sum;
 
    if (val > 255) {
        val = 255;
    }

    return val;
}

/**
  * This function blurs and image
  * 
  * @param the image to be altered
  * @param the sigma value
  * @return the blurred image.
  */
Image * blur(Image * im, double sigma) {

  	double **g = gaussian(sigma);
  	Image * retIm = malloc(sizeof(Image));
	  Pixel * pix = malloc(sizeof(Pixel) * im->x * im->y);
	  retIm->data = pix;
  	retIm->x = im->x;
  	retIm->y = im->y;

    for (int row = 0; row < im->y; row++) {
        for (int col = 0; col < im->x; col++) {
            retIm->data[(row * retIm->x) + col].r = filterResponse(g,im,row,col,'r',sigma);
            retIm->data[(row * retIm->x) + col].b = filterResponse(g,im,row,col,'b',sigma);
            retIm->data[(row * retIm->x) + col].g = filterResponse(g,im,row,col,'g',sigma);
        }
    }  

  
    int N = sigma * 10;
    if (N % 2 == 0) {
        N++;
    }

    for (int i = 0; i < N; i++) {
        free(g[i]);
    }

    free(g);
    free(im->data);
    free(im);
  
    return retIm;
}

/**
  * This function finds the egdes of an image
  * 
  * @param the image to be altered.
  * @param the sigma value
  * @param the threshold value
  * @return the resulting image
  */
Image * edgeDetection (Image * img, double sigma, double thresh) {

    int rows = img->y;
    int cols = img->x;
    img = grayscale(img);
    img = blur(img, sigma);
 
    int **intensex = malloc(sizeof(int*) * rows);
    if (!intensex) {
  	    printf("didn't allocate properly\n");
    }

    for (int i = 0; i < rows; i++) {
        intensex[i] = malloc(sizeof(int) * cols);
    }

    int **intensey = malloc(sizeof(int*) * rows);
    if (!intensey) {
        printf("didn't allocate properly\n");
    }

    for (int i = 0; i < rows; i++) {
        intensey[i] = malloc(sizeof(int) * cols);
    }

    double **intenseMag = malloc(sizeof(double*) * rows);
    if (!intenseMag) {
        printf("didn't allocate properly\n");
    }

    for (int i = 0; i < rows; i++) {
        intenseMag[i] = malloc(sizeof(double) * cols);
    }


    for (int i = 1; i < img->y - 1; i++) {
        for (int j = 1; j < img->x-1; j++) {
            intensex[i][j] = (img->data[((i+1) * img->x) + j].r -
			                        img->data[((i-1) * img->x) + j].r)/2;
      
            intensey[i][j] = (img->data[(i * img->x) + (j+1)].r -
			                        img->data[(i * img->x) + (j-1)].r)/2;

            intenseMag[i][j] = sqrt(sq(intensex[i][j]) + sq(intensey[i][j]));
        }
    }

    for (int i = 1; i < img->y - 1; i++) {
        for (int j = 1; j < img->x-1; j++) {

            if (intenseMag[i][j] > thresh) {
                img->data[(i * img->x) + j].r = 0;
                img->data[(i * img->x) + j].g = 0;
                img->data[(i * img->x) + j].b = 0;
            } else {
                img->data[(i * img->x) + j].r = 255;
                img->data[(i * img->x) + j].g = 255;
                img->data[(i * img->x) + j].b = 255;
            }
        }
    }

    for (int i = 0; i < rows; i++) {
        free(intensex[i]);
        free(intensey[i]);
        free(intenseMag[i]);
    }

    free(intensex);
    free(intensey);
    free(intenseMag);
   
    return img;
}

