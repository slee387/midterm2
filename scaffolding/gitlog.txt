//Eliza Cohn
//ecohn4
//Soo Hyun Lee
//slee387
//601.220, Spring 2019
//gitlog.txt


commit 6c9fc34495aedd7d039c9d88d3ffda50e25c7c15
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Thu Mar 28 14:49:23 2019 -0400

    everything is done and tested!

commit e3437fcdf4d3b9ae3ab8a18014e5b964063e88e5
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Thu Mar 28 13:46:47 2019 -0400

    cleaned things up

commit f9c6bb329bab9d6fb20ab4534d11a1c7c2f19ca1
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Wed Mar 27 19:33:26 2019 -0400

    Valgrind tested and works

commit f87a34b095167dcf57699f29e1cd16a808fcd288
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Wed Mar 27 19:14:38 2019 -0400

    sanity check...

commit 868fddd2242fccc4e4742a9a1416060afda9bec6
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Wed Mar 27 18:23:52 2019 -0400

    lets do valgrind

commit b7288f109bf7ffbdcec8b41540041bbfeb76b630
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Wed Mar 27 18:06:50 2019 -0400

    edges are workinggit add imageManip.c project.c

commit 5fa81ce86041108ec69d1716dd755ed506790b85
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Wed Mar 27 00:38:31 2019 -0400

    pushing all the filesss

commit 1bd2871d842fbfb4aa37a6de88c5fd42d6b12ecd
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Tue Mar 26 22:17:59 2019 -0400

    it creates good images now

commit ebb496ade25a76c4fb94ad05a285c47f6bb1d850
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Tue Mar 26 17:08:41 2019 -0400

    makefile done

commit c8230392fcba49983d834d6b8fe6d3fb28ca932e
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Tue Mar 26 16:16:46 2019 -0400

    edge works

commit add773df07dc1ac6012844484104d0237cfbe9dc
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Tue Mar 26 14:18:02 2019 -0400

    blur works!

commit cd8f5d9d17c7079211ac2ed215fd0c47f57fb5e0
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Mon Mar 25 23:15:05 2019 -0400

    more blur

commit 2714571b1678b344dd62feef62394d14b355b09d
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Mon Mar 25 22:39:56 2019 -0400

    added crop

commit 6051d605ccf8c4edc964576687a5766d8ecba2e7
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Mon Mar 25 22:33:31 2019 -0400

    worked on blur

commit 193b2ca876c90018fa418f96fe7c30070d35ce48
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Mon Mar 25 22:09:46 2019 -0400

    blur test

commit 7c739d10afe963f25eb744f76eaa650bc1ef9235
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Mon Mar 25 19:59:15 2019 -0400

    added image manipulation

commit 773d0dfb1d21a1119f4cf36ad7e83e4804fff35e
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Mon Mar 25 19:42:10 2019 -0400

    ppm cleaned up

commit 62859008c4cb6a6684ed3b94c9294575fbcb2c15
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Mon Mar 25 19:39:01 2019 -0400

    with comments

commit bc89e6ef4e65e2b67bd29ff13f4b734e3e032b8b
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Mon Mar 25 11:50:58 2019 -0400

    added more functions

commit 5c38f7837e13e21840eedf54e2dbb6ae2738c229
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Mon Mar 25 09:50:14 2019 -0400

    added driver program and linking

commit 4d057ae235740d5854e446cb14e0008b5151e739
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Sun Mar 24 20:15:54 2019 -0400

    adding blur

commit a6beb34dc0fc79b143314f39522c472e1534b78f
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Sun Mar 24 20:10:55 2019 -0400

    adding ppm_io.h

commit acda144490065fd612debcc3c7d4a77372244809
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Sun Mar 24 18:32:37 2019 -0400

    it reads and writesgit add ppm_io.c

commit a6f38affeb4f60914a6b07a5e2322302fa787f9f
Author: Soo Hyun Lee <Soohyun@Soos-MacBook-Pro-3.local>
Date:   Sun Mar 24 18:05:03 2019 -0400

    the basics work

commit b5dec642f179273ec16988ce729ac6ae023e0db2
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Fri Mar 1 17:48:02 2019 -0500

    testiing to add

commit 502aba826660f8dfbcae9fb78d93cebda67961cb
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Fri Mar 1 17:17:56 2019 -0500

    test

commit 2e72f2e2aa134c8e35d0cbe43afec3e33cf75d8a
Author: Eliza Cohn <ecohn4@jhu.edu>
Date:   Fri Mar 1 22:05:22 2019 +0000

    README.md created online with Bitbucket
