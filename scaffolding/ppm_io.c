// Soo Hyun Lee (slee387)
// Eliza Cohn (ecohn4)
// ppm_io.c
// 601.220, Spring 2019

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>
#include <stdio.h>

Image * read_ppm(FILE *fp) {

    // Checking fp isn't null once again
    assert(fp);

    Image *img;
    char p;
    int six, rgb;

    // Checking if the format is P6
    if (fscanf(fp, " %c%d", &p, &six) == 2){
        if(p != 'P') {
            printf("error");
        }

        if (six != 6) {
            printf("error");
        }
    }

    // Check for comments & do nothing about them
    // Just moving the pointers 
    p = fgetc(fp);
    p = fgetc(fp);

    if (p == '#') {
        while (p != '\n') {
            p = fgetc(fp);
        }
    } else {
        rewind(fp);
        p = fgetc(fp);
        p = fgetc(fp);
        p = fgetc(fp);
    }


    // Allocate memory for image
    img = (Image *)malloc(sizeof(Image));
    if (!img) {
        printf("Memory allocation failed\n");
        return NULL; 
    }

    // Reading cols & rows
    if (fscanf(fp, " %d %d", &img->x, &img->y) != 2) {
        printf("%d %d", img->x, img->y);
        printf("Invalid size of image\n");
        return NULL; 
    }

    // Reading rgb values
    if (fscanf(fp, " %d", &rgb) != 1) {
        printf("Invalid rgb value.\n");
        return NULL;  
    }

    p = fgetc(fp);

    // If value isn't 255, can't proceed
    if (rgb != 255) {
        printf("Not an rgb value.\n");
        return NULL; 
    }


    // Allocating memory for pixels
    Pixel * pix = malloc(sizeof(Pixel) * img->x * img->y);
    if (!pix) {
        printf("Pixel array allocation failed.\n");
        free(img->data);
        free(img);
        return NULL;
    }

    img->data = pix;

    fread(img->data, 3 * img->x, img->y, fp);
    if (feof(fp)) {
        printf("pointer got to the end\n");
    }

    fclose(fp);
    return img;  

}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

    // check that fp is not NULL
    assert(fp); 

    if (!fp) {
        return -1;
    }

    if (!im) {
        return -1;
    }

    // writing header files 
    fprintf(fp, "P6\n%d %d\n255\n", im->x, im->y);

    // now write the pixel array
    int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->x * im->y, fp);

    if (num_pixels_written != im->x * im->y) {
        return -1;
    }

    fclose(fp);
    return num_pixels_written;

}


